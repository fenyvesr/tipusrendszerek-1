Az alábbi listában megtalálható, hogy melyik típusrendszernek melyik tételét kell bebizonyítanod.

Töltsd fel a megoldást PDF formátumban!

Indukció esetén elég az első 4 esetet belátni, nem kell az összeset (a beadandó lényege, hogy megértsétek, hogy mi az a strukturális/levezetés szerinti indukció).

Az adott tétel bizonyításához a többi tételt fel lehet használni.

Aki LaTeX/ben szeretné megírni, annak itt megtalálható (Linkek egy külső oldalra) a különböző feladatok forráskódja.

Ha valami hiba van, jelezzétek!
