module NatBool where

open import lib

infix  2 _∶_

-- 2.1

data Tm         : Set where
  num           : ℕ → Tm
  _+Tm_         : (t t' : Tm) → Tm
  isZero        : (t : Tm) → Tm
  true          : Tm
  false         : Tm
  if_then_else_ : (t t' t'' : Tm) → Tm

num≠+ : {n : ℕ}{t t' : Tm} → num n ≠ t +Tm t'
num≠+ {n}{t}{t'} e = subst P e tt
  where
    P : Tm → Set
    P (num x) = ⊤
    P (t +Tm t') = ⊥
    -- these below don't matter
    P (isZero t) = ⊤
    P true = ⊤
    P false = ⊤
    P (if t then t₁ else t₂) = ⊤

height : Tm → ℕ
height (num n) = 0
height (t +Tm t') = 1 + max (height t) (height t')
height (isZero t) = 1 + height t
height true = 0
height false = 0
height (if t then t' else t'') = 1 + max (height t) (max (height t') (height t''))

trues : Tm → ℕ
trues (num n) = 0
trues (t +Tm t') = trues t + trues t'
trues (isZero t) = trues t
trues true = 1
trues false = 0
trues (if t then t' else t'') = trues t + trues t' + trues t''

lemma : (t : Tm) → trues t ≤ 3 ^ height t
lemma (num n) = z≤n
lemma (t +Tm t') =
  ≤-trans {trues t + trues t'}
                (+≤+ (lemma t) (lemma t'))
                (subst ((3 ^ height t) + (3 ^ height t') ≤_) (+-assoc x x (x + 0)) almost)
  where
    x = 3 ^ max (height t) (height t')
    h = max≤ (height t) (height t')
    almost = ≤mon+ (+≤+ (≤mon^ (proj₁ h)) (≤mon^ (proj₂ h)))
lemma (isZero t) = ≤mon+ (lemma t)
lemma true = s≤s z≤n
lemma false = z≤n
lemma (if t then t' else t'') =
  ≤-trans {trues t + trues t' + trues t''}
                (+≤+ (+≤+ (lemma t) (lemma t')) (lemma t''))
                (subst ((3 ^ height t) + (3 ^ height t') + (3 ^ height t'') ≤_) ≡lemma almost)
  where
    x = 3 ^ max (height t) (max (height t') (height t''))
    h = max≤' (height t) (height t') (height t'')
    h₁ = proj₁ h
    h₂ = proj₁ (proj₂ h)
    h₃ = proj₂ (proj₂ h)
    almost : (3 ^ height t  + 3 ^ height t' + 3 ^ height t'' ≤ x + x + x)
    almost = +≤+ (+≤+ (≤mon^ h₁) (≤mon^ h₂)) (≤mon^ h₃)
    ≡lemma : x + x + x ≡ x + (x + (x + 0))
    ≡lemma = subst (λ y → x + x + y ≡ x + (x + (x + 0))) (+-comm x 0) (+-assoc x x (x + 0))

-- 2.2

data Ty : Set where
  Nat Bool : Ty

RecTy : ∀{ℓ}{A : Set ℓ} → A → A → Ty → A
RecTy a a' Nat  = a
RecTy a a' Bool = a'

IndTy : ∀{ℓ}(P : Ty → Set ℓ)(pNat : P Nat)(pBool : P Bool)(A : Ty) → P A
IndTy P pNat pBool Nat  = pNat
IndTy P pNat pBool Bool = pBool

record TyPat {i} (P : Ty → Set i) : Set i where
  field
    pNat : P Nat
    pBool : P Bool

matchTy : ∀{i}(P : Ty → Set i)(p : TyPat P)(A : Ty) → P A
matchTy P p = IndTy P (TyPat.pNat p) (TyPat.pBool p)

data _∶_ : Tm → Ty → Set where
  num:Nat : {n : ℕ} → num n ∶ Nat
  plus:Nat : {t : Tm} → (t ∶ Nat) → {t' : Tm} → (t' ∶ Nat) → t +Tm t' ∶ Nat
  isZero:Bool : {t : Tm} → (t ∶ Nat) → isZero t ∶ Bool
  true:Bool : true ∶ Bool
  false:Bool : false ∶ Bool
  if:A : {t : Tm} → (t ∶ Bool) →
         {A : Ty} →
         {t' : Tm} → (t' ∶ A) →
         {t'' : Tm} → (t'' ∶ A) →
         if t then t' else t'' ∶ A

⟦_⟧ : {t : Tm} → {A : Ty} → t ∶ A → ℕ
⟦ num:Nat {n} ⟧ = n
⟦ plus:Nat t:Nat t':Nat ⟧ = ⟦ t:Nat ⟧ + ⟦ t':Nat ⟧
⟦ isZero:Bool (t:Nat) ⟧ with ⟦ t:Nat ⟧
⟦ isZero:Bool (t:Nat) ⟧ | 0 = 1
⟦ isZero:Bool (t:Nat) ⟧ | suc _ = 0
⟦ true:Bool ⟧ = 1
⟦ false:Bool ⟧ = 2
⟦ if:A t:Bool t':A t'':A ⟧ with ⟦ t:Bool ⟧
⟦ if:A t:Bool t':A t'':A ⟧ | 1 = ⟦ t':A ⟧
⟦ if:A t:Bool t':A t'':A ⟧ | 0 = ⟦ t'':A ⟧
⟦ if:A t:Bool t':A t'':A ⟧ | suc (suc _) = ⟦ t'':A ⟧

unicity : {t : Tm} → {A : Ty} → {A' : Ty} → (t ∶ A) → (t ∶ A') → A ≡ A'
unicity num:Nat num:Nat = refl
unicity (plus:Nat t:A t:A₁) (plus:Nat t:A' t:A'') = refl
unicity (isZero:Bool t:A) (isZero:Bool t:A') = refl
unicity true:Bool true:Bool = refl
unicity false:Bool false:Bool = refl
unicity {t}{A}{A'}(if:A t:A t:A₁ t:A₂) (if:A t:A' t:A'' t:A''') = unicity t:A₁ t:A''

nontrivial : Σ Tm λ t → ¬ (Σ Ty λ A → t ∶ A)
nontrivial = (isZero true) , λ { (Nat , ()) ; (Bool , isZero:Bool ()) }



infer : Tm → Maybe Ty
infer (num x) = just Nat
infer (t +Tm t') = maybe (RecTy (maybe (RecTy (just Nat) nothing) nothing (infer t')) nothing)
                         nothing
                         (infer t)
infer (isZero t) = maybe (RecTy (just Bool) nothing) nothing (infer t)
infer true = just Bool
infer false = just Bool
infer (if t then t' else t'') =
  maybe (λ _ → maybe (λ ty → require ty (infer t''))
                     nothing
                     (infer t'))
        nothing
        (require Bool (infer t))
  where
  require : Ty → Maybe Ty → Maybe Ty
  require ty ma = RecTy (maybe (RecTy (just Nat) nothing) nothing ma)
                        (maybe (RecTy nothing (just Bool)) nothing ma)
                        ty

soundness : (t : Tm)(A : Ty) → infer t ≡ just A → t ∶ A
soundness (num x) .Nat refl = num:Nat
soundness (t +Tm t') A e =
  IndMaybe
    (λ ma → infer t ≡ ma → (maybe (RecTy (maybe (RecTy (just Nat) nothing) nothing (infer t')) nothing) nothing ma ≡ just A) → (t +Tm t') ∶ A)
    (IndTy (λ A' → infer t ≡ just A' → RecTy (maybe (RecTy (just Nat) nothing) nothing (infer t')) nothing A' ≡ just A → (t +Tm t') ∶ A)
           (λ infert= → IndMaybe (λ ma → infer t' ≡ ma → maybe (RecTy (just Nat) nothing) nothing ma ≡ just A → (t +Tm t') ∶ A)
                                 (IndTy (λ A' → infer t' ≡ just A' → RecTy (just Nat) nothing A' ≡ just A → (t +Tm t') ∶ A)
                                        (λ infert'= → λ { refl → plus:Nat (soundness t Nat infert=) (soundness t' Nat infert'=) })
                                        λ _ ())
                                 (λ _ ())
                                 (infer t')
                                 refl)
           λ _ ())
    (λ _ ())
    (infer t) refl e
{-
soundness (isZero t) A e with infer t | inspect' (hide infer t)
soundness (isZero t) .Bool refl | just Nat | it y = {!!} -- y should be of type (infer t ≡ just Nat) but the left hand side is also normalized?
soundness (isZero t) A e | just Bool | it y = {!!}
soundness (isZero t) A e | nothing | it x = {!!} -}
soundness (isZero t) A e with inspect (infer t)
soundness (isZero t) A e | just Nat with≡ eq =
  subst (λ A → isZero t ∶ A)
         (cong (maybe idfun Nat)
               (subst (λ infert → maybe (RecTy (just Bool) nothing) nothing infert ≡ just A)
               eq
               e))
         (isZero:Bool (soundness t _ eq))
soundness (isZero t) A e | just Bool with≡ eq with infer t
soundness (isZero t) A () | just Bool with≡ refl | just .Bool
soundness (isZero t) A e | just Bool with≡ () | nothing
soundness (isZero t) A e | nothing with≡ eq with infer t
soundness (isZero t) A e | nothing with≡ () | just x
soundness (isZero t) A () | nothing with≡ refl | nothing
soundness true .Bool refl = true:Bool
soundness false .Bool refl = false:Bool
soundness (if t then t' else t'') A e with inspect (infer t)
soundness (if t then t' else t'') A e    | just Nat with≡ eq with infer t
soundness (if t then t' else t'') A ()   | just Nat with≡ eq    | just Nat
soundness (if t then t' else t'') A e    | just Nat with≡ ()    | just Bool
soundness (if t then t' else t'') A e    | just Nat with≡ ()    | nothing
soundness (if t then t' else t'') A e    | just Bool with≡ eq = {!!}
soundness (if t then t' else t'') A e    | nothing with≡ eq with infer t
soundness (if t then t' else t'') A e    | nothing with≡ ()    | just x
soundness (if t then t' else t'') A ()   | nothing with≡ eq    | nothing

completeness : (t : Tm)(A : Ty) → (t ∶ A) → infer t ≡ just A
completeness .(num _) .Nat num:Nat = refl
completeness .(_ +Tm _) .Nat (plus:Nat {t} t:A {t'} t:A') =
  subst (λ mA → maybe (RecTy (maybe (RecTy (just Nat) nothing) nothing (infer t')) nothing) nothing mA ≡ just Nat)
         (sym (completeness _ _ t:A))
         (subst (λ mA → maybe (RecTy (just Nat) nothing) nothing mA ≡ just Nat)
                (sym (completeness _ _ t:A'))
                refl)
completeness (isZero .t) .Bool (isZero:Bool {t} t:A) =
  subst (λ infert → maybe (RecTy (just Bool) nothing) nothing infert ≡ just Bool)
        (sym (completeness _ _ t:A))
        refl
completeness .true .Bool true:Bool = refl
completeness .false .Bool false:Bool = refl
completeness .(if t then t' else t'') .A (if:A {t} t:A {A = A}{t' = t'} t:A' {t''} t:A'') =
  subst (λ infert → goal infert (infer t') (infer t''))
        (sym (completeness _ _ t:A))
        {!!}
  where
    goal : Maybe Ty → Maybe Ty → Maybe Ty → Set
    goal infert infert' infert'' =
      maybe
      (λ _ →
         maybe
         (λ ty →
            RecTy (maybe (RecTy (just Nat) nothing) nothing (infert''))
            (maybe (RecTy nothing (just Bool)) nothing (infert'')) ty)
         nothing (infert'))
      nothing (maybe (RecTy nothing (just Bool)) nothing (infert))
      ≡ just A
