{-# OPTIONS --without-K --rewriting #-}

module lib where

-- open impor⟦ isZero:Nat (num:Nat {zero}) ⟧ =t Agda.Primitive renaming (⊔ to ⊔')
-- open import Agda.Primitive
import Agda.Primitive

infixr 1 _⊎_
infixr 2 _≡⟨_⟩_ _≤⟨_⟩_
infix  3 _∎ _≤_ _∎'
infix  4 _≠_ _≡_
infixr 4 _×_
infixr 5 _,_
infixl 6 _⊔_
infixl 7 _⊓_
infixr 8 _^_

------------------------------------------------------------------------------
-- functions
------------------------------------------------------------------------------

idfun : ∀{ℓ}{A : Set ℓ} → A → A
idfun x = x

------------------------------------------------------------------------------
-- equality
------------------------------------------------------------------------------

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Set ℓ where
  refl : x ≡ x

{-# BUILTIN REWRITE _≡_ #-}
{- BUILTIN EQUALITY _≡_ -}

trans : ∀{ℓ}{A : Set ℓ}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans refl refl = refl

sym : ∀{ℓ}{A : Set ℓ}{x y : A} → x ≡ y → y ≡ x
sym refl = refl

_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = trans x≡y y≡z

_∎ : ∀{ℓ}{A : Set ℓ}(x : A) → x ≡ x
x ∎ = refl

subst : ∀{i j}{A : Set i}(P : A → Set j){x y : A} → x ≡ y → P x → P y
subst P refl q = q

cong  : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
cong f refl = refl

------------------------------------------------------------------------------
-- sigma
------------------------------------------------------------------------------

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ Agda.Primitive.⊔ ℓ') where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σ public

-- nondependent

_×_ : ∀{ℓ ℓ'} → Set ℓ → Set ℓ' → Set (ℓ Agda.Primitive.⊔ ℓ')
A × B = Σ A λ _ → B

------------------------------------------------------------------------------
-- top
------------------------------------------------------------------------------

record ⊤ : Set where
  constructor tt

set⊤ : {x y : ⊤}(p q : x ≡ y) → p ≡ q
set⊤ refl refl = refl

------------------------------------------------------------------------------
-- bottom
------------------------------------------------------------------------------

data ⊥ : Set where

⊥-elim : ∀{ℓ}{A : Set ℓ} → ⊥ → A
⊥-elim ()

¬ : ∀{ℓ} → Set ℓ → Set ℓ
¬ A = A → ⊥

_≠_ : ∀{ℓ}{A : Set ℓ} → A → A → Set ℓ
a ≠ a' = ¬ (a ≡ a')

------------------------------------------------------------------------------
-- disjoint union
------------------------------------------------------------------------------

data _⊎_ (A B : Set) : Set where
  inl : A → A ⊎ B
  inr : B → A ⊎ B

ind⊎ : {A B : Set}(P : A ⊎ B → Set) → ((a : A) → P (inl a)) → ((b : B) → P (inr b))
     → (w : A ⊎ B) → P w
ind⊎ P ca cb (inl a) = ca a
ind⊎ P ca cb (inr b) = cb b

------------------------------------------------------------------------------
-- natural numbers
------------------------------------------------------------------------------

open import Agda.Builtin.Nat public
  using    ( zero; suc; _+_; _*_ )
  renaming ( Nat to ℕ
           ; _-_ to _∸_ )

data _≤_ : ℕ → ℕ → Set where
  z≤n : ∀ {n}                 → zero  ≤ n
  s≤s : ∀ {m n} (m≤n : m ≤ n) → suc m ≤ suc n

ind : (P : ℕ → Set)(z : P zero)(s : {n : ℕ} → P n → P (suc n))(n : ℕ) → P n
ind P z s zero = z
ind P z s (suc n) = s (ind P z s n)

------------------------------------------------------------------------
-- Arithmetic

pred : ℕ → ℕ
pred zero    = zero
pred (suc n) = n

-- Max.

max : ℕ → ℕ → ℕ
max zero zero = zero
max zero (suc m) = suc m
max (suc n) zero = suc n
max (suc n) (suc m) = suc (max n m)

_⊔_ : ℕ → ℕ → ℕ
zero  ⊔ n     = n
suc m ⊔ zero  = suc m
suc m ⊔ suc n = suc (m ⊔ n)

-- Min.

_⊓_ : ℕ → ℕ → ℕ
zero  ⊓ n     = zero
suc m ⊓ zero  = zero
suc m ⊓ suc n = suc (m ⊓ n)

-- Naïve exponentiation

_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n

------------------------------------------------------------------------
-- Properties of _+_

-- Algebraic properties of _+_
+-suc : ∀ m n → m + suc n ≡ suc (m + n)
+-suc zero    n = refl
+-suc (suc m) n = cong suc (+-suc m n)

+-assoc : (a b c : ℕ) → a + b + c ≡ a + (b + c)
+-assoc zero    _ _ = refl
+-assoc (suc m) n o = cong suc (+-assoc m n o)

+-identityˡ : (a : ℕ) → 0 + a ≡ a
+-identityˡ _ = refl

+-identityʳ : (a : ℕ) → a + 0 ≡ a
+-identityʳ zero    = refl
+-identityʳ (suc n) = cong suc (+-identityʳ n)

+-comm : (a b : ℕ) → a + b ≡ b + a
+-comm zero    n = sym (+-identityʳ n)
+-comm (suc m) n =
  suc m + n   ≡⟨ refl ⟩
  suc (m + n) ≡⟨ cong suc (+-comm m n) ⟩
  suc (n + m) ≡⟨ sym (+-suc n m) ⟩
  n + suc m   ∎

------------------------------------------------------------------------
-- Properties of _≤_

-- Relation-theoretic properties of _≤_
≤-reflexive : {a b : ℕ} → a ≡ b → a ≤ b
≤-reflexive {zero}  refl = z≤n
≤-reflexive {suc m} refl = s≤s (≤-reflexive refl)

≤-refl : {a : ℕ} → a ≤ a
≤-refl = ≤-reflexive refl

≤-antisym : {a b : ℕ} → a ≤ b → b ≤ a → a ≡ b
≤-antisym z≤n       z≤n       = refl
≤-antisym (s≤s m≤n) (s≤s n≤m) with ≤-antisym m≤n n≤m
... | refl = refl

≤-trans : {a b c :  ℕ} → a ≤ b → b ≤ c → a ≤ c
≤-trans z≤n       _         = z≤n
≤-trans (s≤s m≤n) (s≤s n≤o) = s≤s (≤-trans m≤n n≤o)

------------------------------------------------------------
-- _≤_ reasoning
_≤⟨_⟩_ : (a : ℕ){b c : ℕ} → a ≤ b → b ≤ c → a ≤ c
a ≤⟨ a≤b ⟩ b≤c = ≤-trans a≤b b≤c

_∎' : (x : ℕ) → x ≤ x
x ∎' = ≤-refl

-- Properties of arithmetics and _≤_
pred-mono : {a b : ℕ} → a ≤ b → pred a ≤ pred b
pred-mono z≤n = z≤n
pred-mono (s≤s q) = q

max≤ : (a b : ℕ) → (a ≤ max a b) × (b ≤ max a b)
max≤ zero zero = z≤n , z≤n
max≤ zero (suc b) = z≤n , ≤-refl
max≤ (suc a) zero = ≤-refl , z≤n
max≤ (suc a) (suc b) = s≤s (proj₁ m) , s≤s (proj₂ m) where m = max≤ a b

max≤' : (a b c : ℕ) → (a ≤ max a (max b c)) × (b ≤ max a (max b c)) × (c ≤ max a (max b c))
max≤' zero zero zero = z≤n , z≤n , z≤n
max≤' zero zero (suc c) = z≤n , (z≤n , (s≤s ≤-refl))
max≤' zero (suc b) zero = z≤n , ((s≤s ≤-refl) , z≤n)
max≤' zero (suc b) (suc c) = z≤n , ((s≤s (proj₁ h)) , s≤s ((proj₂ h))) where h = max≤ b c
max≤' (suc a) zero zero = (s≤s ≤-refl) , (z≤n , z≤n)
max≤' (suc a) zero (suc c) = (s≤s (proj₁ h)) , z≤n , (s≤s (proj₂ h)) where h = max≤ a c
max≤' (suc a) (suc b) zero = (s≤s (proj₁ h)) , (s≤s (proj₂ h)) , z≤n where h = max≤ a b
max≤' (suc a) (suc b) (suc c) = (s≤s (proj₁ h)) , ((s≤s (proj₁ (proj₂ h))) , (s≤s (proj₂ (proj₂ h)))) where h = max≤' a b c

{-
_^_ : ℕ → ℕ → ℕ
n ^ zero = 1
n ^ suc m = n * n ^ m
-}

≤add : {a b : ℕ} →  a ≤ a + b
≤add {zero} {b} = z≤n
≤add {suc a} {b} = s≤s ≤add

≤mon+ : {a b c : ℕ} → a ≤ b → a ≤ b + c
≤mon+ {a} {b} {c} q = ≤-trans {a} {b} {b + c} q ≤add

≤mon^l1 : {n : ℕ} → 1 ≤ 3 ^ (n + 1)
≤mon^l1 {zero} = s≤s z≤n
≤mon^l1 {suc n} = ≤mon+ (≤mon^l1 {n})

+≤+ : {a b a' b' : ℕ} → a ≤ b → a' ≤ b' → a + a' ≤ b + b'
+≤+ z≤n z≤n = z≤n
+≤+ (z≤n {a}) (s≤s {b}{c} q) = subst (suc b ≤_) (+-comm (suc c) a) (s≤s (≤-trans {b} {c} {c + a} q ≤add))
+≤+ (s≤s p) z≤n = s≤s (+≤+ p z≤n)
+≤+ (s≤s p) (s≤s q) = s≤s (+≤+ p (s≤s q))

+≤ : {a' a'' a b b' : ℕ} → a' ≤ a'' → a + b ≤ a' + b' → a + b ≤ a'' + b'
+≤ q r = ≤-trans r (+≤+ q ≤-refl)  

≤mon^ : {a b : ℕ} → a ≤ b → 3 ^ a ≤ 3 ^ b
≤mon^ {zero} {zero} = s≤s
≤mon^ {zero} {suc b} = λ _ → ≤mon+ {1} {3 ^ b} (≤mon^ (z≤n {b}))
≤mon^ {suc a} {zero} = λ ()
≤mon^ {suc a} {suc b} q = +≤+ r (+≤+ r (+≤+ r z≤n)) where r = ≤mon^ {a} {b} (pred-mono q)

---------------------------------------
-- Maybe
---------------------------------------

data Maybe (A : Set) : Set where
  just : A  → Maybe A
  nothing : Maybe A

maybe : {A B : Set} → (A → B) → B → Maybe A → B
maybe f _ (just x) = f x
maybe f b nothing = b

mapMaybe : {A B : Set} → (A → B) → Maybe A → Maybe B
mapMaybe f ma = maybe (λ x → just (f x)) nothing ma

_>>=M_ : {A B : Set} → Maybe A → (A → Maybe B) → Maybe B
x >>=M f = maybe f nothing x

IndMaybe : {A : Set}(P : Maybe A → Set)(pjust : (a : A) → P (just a))(pnothing : P nothing)
  (ma : Maybe A) → P ma
IndMaybe P pjust pnothing (just a) = pjust a
IndMaybe P pjust pnothing nothing = pnothing

record MaybePat {i}{A : Set}(P : Maybe A → Set i) : Set i where
  field
    pjust : (a : A) → P (just a)
    pnothing : P nothing

matchMaybe : {A : Set} (P : Maybe A → Set)(p : MaybePat P)(ma : Maybe A) → P ma
matchMaybe P p = IndMaybe P (MaybePat.pjust p) (MaybePat.pnothing p)

---------------------------------------
-- Singleton
---------------------------------------

data Singleton {a} {A : Set a} (x : A) : Set a where
  _with≡_ : (y : A) → x ≡ y → Singleton x

inspect : ∀ {a} {A : Set a} (x : A) → Singleton x
inspect x = x with≡ refl

---------------------------------------
-- Inspect on steroids - https://lists.chalmers.se/pipermail/agda/2011/003286.html
---------------------------------------

-- The key to steroid-infused inspect is being able to hide terms from the evil with-abstraction monster.

-- The type of hidden terms.
Hidden : Set → Set
Hidden A = ⊤ → A

-- We can hide a function application
hide : {A : Set}{B : A → Set} (f : (x : A) → B x) (x : A) → Hidden (B x)
hide f x tt = f x

-- Revealing a hidden argument is easy.
reveal : {A : Set} → Hidden A → A
reveal f = f tt

-- Inspect on steroids relates hidden values and the corresponding
-- revealed values.
-- (I'm resisting the temptation to call it reveal_is_).

data Inspect {A : Set}(x : Hidden A)(y : A) : Set where
  it : reveal x ≡ y → Inspect x y

-- It's easy to show that revealing x gives you reveal x:
inspect' : {A : Set}(x : Hidden A) → Inspect x (reveal x)
inspect' f = it refl
