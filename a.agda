module a where

open import lib

module section2 where

  record Variable : Set₁ where
    field
      Name : Set
      dec : (x x' : Name) → isDec (x ≡ x')
      newName : List Name → Name
      uniq : (xs : List Name) → newName xs ∈ xs → ⊥

  open Variable

  postulate
    VNat : Variable
    VExp : Variable

--  data Var (A : Set) : Set where
--    v : ℕ → Var A

  data Nat : Set where
    var : Name VNat → Nat
    zero : Nat
    suc : Nat → Nat

  data Exp : Set where
    var : Name VExp → Exp
    num : Nat → Exp
    _+_ : Exp → Exp → Exp

  ex1 : Nat
  ex1 = suc (suc zero)

  x : Name VExp
  x = newName VExp []

  i : Name VNat
  i = newName VNat []

  ex2 : Exp
  ex2 = num (suc zero) + ((var x + (num zero)) + num (suc (var i)))

  _[_↦_]EE : Exp → Name VExp → Exp → Exp
  _[_↦_]EN : Exp → Name VNat → Nat → Exp
  _[_↦_]NN : Nat → Name VNat → Nat → Nat

  var x [ x' ↦ e' ]EE with dec VExp x x'
  var x [ x' ↦ e' ]EE | yes _ = e'
  var x [ x' ↦ e' ]EE | no _ = var x
  num n [ x' ↦ e' ]EE = num n
  (e₁ + e₂) [ x ↦ e' ]EE = (e₁ [ x ↦ e' ]EE) + (e₂ [ x ↦ e' ]EE)
  
  var x [ i ↦ n ]EN = var x
  num n [ i ↦ n' ]EN = num (n [ i ↦ n' ]NN)
  (e₁ + e₂) [ i ↦ n ]EN = (e₁ [ i ↦ n ]EN) + (e₂ [ i ↦ n ]EN)
  
  var i [ i' ↦ n' ]NN with dec VNat i i'
  var i [ i' ↦ n' ]NN | yes _ = n'
  var i [ i' ↦ n' ]NN | no _ = var i
  zero [ i ↦ n' ]NN = zero
  suc n [ i ↦ n' ]NN = suc (n [ i ↦ n' ]NN)

  ex3 : {x : Name VExp}
      → (var x + num zero) [ x ↦ num (suc zero) ]EE ≡ num (suc zero) + num zero
  ex3 {x} with dec VExp x x
  ex3 | yes refl = refl
  ex3 | no q = ⊥-elim (q refl)

  ex4 : {x x' : Name VExp}
      → (var x + var x) [ x ↦ var x' + num zero ]EE ≡ (var x' + num zero) + (var x' + num zero)
  ex4 {x} with dec VExp x x
  ex4 | yes refl = refl
  ex4 | no q = ⊥-elim (q refl)

  ex5 : {x : Name VExp}{i : Name VNat}
      → (var x + num (suc (var i))) ≡ {!!}
  ex5 = {!!}

{-

module section2 where

  -- inductive definitions

  record Var : Set₁ where
    field
      Name : Set
      dec : (x x' : Name) → isDec (x ≡ x')
      newName : List Name → Name
      uniq : (xs : List Name) → newName xs ∈ xs → ⊥

  open Var

  postulate
    VarNat : Var
    VarExp : Var

  data Nat : Set where
    var : Name VarNat → Nat
    zero : Nat
    suc : Nat → Nat

  x x' : Name VarExp
  x = newName VarExp []
  x' = newName VarExp (x ∷ [])

  i : Name VarNat
  i = newName VarNat []

  module subsection2-1 where

    -- abstract syntax trees

    data Exp : Set where
      var : Name VarExp → Exp
      num : Nat → Exp
      _+_ : Exp → Exp → Exp

    infix 5 _+_

    exa1 : Nat
    exa1 = suc (suc zero)

    exa2 : Exp
    exa2 = num (suc zero) + ((var x + num zero) + num (suc (var i)))

    infix 3 _[_↦_]Nat _[_↦_]ExpNat _[_↦_]ExpExp

    _[_↦_]Nat : Nat → Name VarNat → Nat → Nat
    var i' [ i ↦ n' ]Nat with dec VarNat i' i
    var i' [ .i' ↦ n' ]Nat | yes refl = n'
    var i' [ i ↦ n' ]Nat | no q = var i'
    zero [ i ↦ n' ]Nat = zero
    suc n [ i ↦ n' ]Nat = suc (n [ i ↦ n' ]Nat)

    _[_↦_]ExpNat : Exp → Name VarNat → Nat → Exp
    var x [ i ↦ n ]ExpNat = var x
    num n' [ i ↦ n ]ExpNat = num (n' [ i ↦ n ]Nat)
    (e + e₁) [ i ↦ n ]ExpNat = (e [ i ↦ n ]ExpNat) + (e₁ [ i ↦ n ]ExpNat)
    
    _[_↦_]ExpExp : Exp → Name VarExp → Exp → Exp
    var x [ x₁ ↦ e' ]ExpExp with dec VarExp x x₁
    var x [ .x ↦ e' ]ExpExp | yes refl = e'
    var x [ x₁ ↦ e' ]ExpExp | no q = var x
    num n [ x₁ ↦ e' ]ExpExp = num n
    (e + e₁) [ x ↦ e' ]ExpExp = (e [ x ↦ e' ]ExpExp) + (e₁ [ x ↦ e' ]ExpExp)

    exa3 : ((var x + num zero) [ x ↦ num (suc zero) ]ExpExp)
         ≡ (num (suc zero) + num zero)
    exa3 with dec VarExp x x
    exa3 | yes refl = refl
    exa3 | no q = ⊥-elim (q refl)

    exa4 : ((var x + var x) [ x ↦ var x' + num zero ]ExpExp)
         ≡ ((var x' + num zero) + (var x' + num zero))
    exa4 with dec VarExp x x
    exa4 | yes refl = refl
    exa4 | no q = ⊥-elim (q refl)

    exa5 : ((var x + num (suc (var i))) [ i ↦ zero ]ExpNat) ≡ var x + num (suc zero)
    exa5 with dec VarNat i i
    exa5 | yes refl = refl
    exa5 | no q = ⊥-elim (q refl)

    -- TODO: exercises

  module subsection2-2 where

    -- abstract binding trees

    data Exp : Set where
      var : Name VarExp → Exp
      num : Nat → Exp
      _+_ : Exp → Exp → Exp
      LET_IN_∙_ : Exp → Name VarExp → Exp → Exp -- \.

    infix 5 _+_

    exa1 : Exp
    exa1 = LET num (suc zero) IN x ∙ (var x + (num zero + var x))
    
    FV : Exp → List (Name VarExp)
    FV (var x) = x ∷ []
    FV (num n) = {!!}
    FV (e + e₁) = {!!}
    FV (LET e IN x ∙ e₁) = {!!}
{-

module b where
  data Var : ℕ → Set where
    vz : {n : ℕ} → Var (suc n)
    vs : {n : ℕ} → Var n → Var (suc n)

  data Exp : ℕ → Set where
    var     : {k : ℕ} → Var k → Exp k
    num     : {k : ℕ} → (n : ℕ) → Exp k
    _+_     : {k : ℕ}(e e' : Exp k) → Exp k
    LET_IN_ : {k : ℕ}(e : Exp k)(e' : Exp (suc k)) → Exp k

  e : Exp zero
  e = LET num zero IN (var vz + (LET var vz IN var (vs vz)))
-}


{-
       n ∈ ℕ
---------------------
leaf n hasHeigth zero

h ∈ ℕ    t,t' ∈ Tree   t hasHeight h    t' hasHeight h
------------------------------------------------------
             node t t' hasHeight (suc h)
-}
{-
data _hasHeight_ : Tree → ℕ → Set where
  leaf : (n : ℕ) → leaf n hasHeight zero
  node : (h : ℕ){t t' : Tree} → t hasHeight h → t' hasHeight h
       → node t t' hasHeight (suc h)
-}

{-
data Tm : ℕ → Set where
  app : {n : ℕ} → Tm n → Tm n → Tm n
  lam : {n : ℕ} → Tm n → Tm (suc n)
  var : {n : ℕ} → Var n → Tm n
-}
-}
